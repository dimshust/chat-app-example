# Simple chat application back-end

The application allows:
1. Create chat rooms by an administrator.
2. Users can connect to chat rooms, send public and private messages in the room.
3. If the user connects to the room with the name which is already exists, he receives a random name.
4. API allows to client make live notifications.

## Build and run:

1. mvn clean install
2. java -jar target/chat-app-1.0-SNAPSHOT.jar

## Implementation
The Spring is used as a base framework for implementation.
The back-end provides 2 types of interfaces:
1. STOMP API over WebSocket - to send and receive messages.
2. REST API which could be used in not related to messaging functionality of a client.
## What could be improved
1. Add security, add authentication.
2. Migrate to more advanced MQ broker.
3. Extend API to provide additional functional e.g. changing nicknames or room names.
4. Extend parameters validation.
5. Add more specific integration and unit tests.
## REST API documentation
To see endpoints input and output there is Swagger:
```
localhost:8080/swagger-ui.html
```
## WorkFlow and Web-socket API
**/v1/chat**

The back-end has a web-socket endpoint /v1/chat. In order connect to this endpoint user has to use STOMP client, 
there are multiple implementations, e.g. JS. The result of a connection is the unique **client-id**
The next step - subscribe to the topic:
+ /topic/**chat-room-name** for public room messages
+ /**user**/topic/**chat-room-name** to get private messages in the room

In order to set the nickname in the room, user can pass a value in to the subscribe header, with the key **room-user-name**.

JS example:

```java
    var socket = new SockJS('/v1/chat');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        client.subscribe("/topic/<room name>", callback, { "room-user-name": "<user name in chat room>" });
        client.subscribe("/user/topic/<room name>", callback, { "room-user-name": "<user name in chat room>" });
    }, errorCallBack);
    client.send("/topic/<chat room name>", header, <json message>);
```
the json message:
```
{
    from: '<client id who sends message>',
    chatRoom: '<chat room name>',
    to: '<optional client id of to whom a private message is sent>',
    text: '<message text>'
}
```
If **to** is empty - message goes to a public room, else it goes to the user directly.

**/v1/userinfo**

Subscription request does not return the result synchronously.
User can send message to web-socket information endpoint: /v1/userinfo.
The request and response messages the same as above. The response.text contains nickname. If during the subscription
nickname was already used by someone else, the nickname will be random. The same information could be obtained through the REST API /api/v1/user

 
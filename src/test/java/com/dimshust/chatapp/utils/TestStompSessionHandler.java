package com.dimshust.chatapp.utils;

import com.dimshust.chatapp.dto.ChatMessageDto;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;

import java.lang.reflect.Type;
import java.util.concurrent.SynchronousQueue;

public class TestStompSessionHandler implements StompSessionHandler {

    private final SynchronousQueue<Object> messageResultQueue;

    public TestStompSessionHandler(SynchronousQueue<Object> messageResultQueue) {
        this.messageResultQueue = messageResultQueue;
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        try {
            messageResultQueue.put(connectedHeaders.get("user-name").get(0));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        System.out.println(exception.getMessage());
    }

    @Override
    public void handleTransportError(StompSession session, Throwable exception) {
        System.out.println(exception.getMessage());
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return ChatMessageDto.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
    }
}
package com.dimshust.chatapp.integration;

import com.dimshust.chatapp.dto.*;
import com.dimshust.chatapp.utils.TestDbCleaner;
import com.dimshust.chatapp.utils.TestRestApiClient;

import com.dimshust.chatapp.utils.TestStompFrameHandler;
import com.dimshust.chatapp.utils.TestStompSessionHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;


import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:test-application.properties")
public class WorkflowIntegrationTest {

    @LocalServerPort
    private int port;

    private static final String CHAT_ROOM_NAME = "TEST_ROOM";
    private static final String FIRST_USER_NAME = "Mike";
    private static final String SECOND_USER_NAME = "John";
    private static final String MESSAGE_TEXT = "TEST";

    private SynchronousQueue<Object> messageResultQueue;
    private TestStompSessionHandler testStompSessionHandler;
    private TestStompFrameHandler testStompFrameHandler;

    @Autowired
    private TestDbCleaner dbTestUtilsService;

    @Autowired
    private TestRestApiClient testRestApiClient;

    @After
    public void clean() {
        dbTestUtilsService.clearChatRooms();
    }

    @Before
    public void setUp() {
        messageResultQueue = new SynchronousQueue<>();
        testStompSessionHandler = new TestStompSessionHandler(messageResultQueue);
        testStompFrameHandler = new TestStompFrameHandler(messageResultQueue);
    }

    @Test
    public void workflowIntegrationTest() throws Exception {

        ResponseEntity<ChatRoomDto> chatRoomResponse = testRestApiClient.createTestRoom(CHAT_ROOM_NAME);
        assertNotNull(chatRoomResponse);
        assertEquals(200, chatRoomResponse.getStatusCodeValue());
        assertEquals(CHAT_ROOM_NAME, chatRoomResponse.getBody().getName());

        WebSocketStompClient stompClient = createStompClient();
        // The first client connects
        StompSession firstUserSession = stompClient.connect("ws://localhost:" + port + "/v1/chat", testStompSessionHandler).get(10, TimeUnit.SECONDS);
        String firstClientId = (String)messageResultQueue.poll(5, TimeUnit.SECONDS);
        assertThat(firstClientId, is(not(isEmptyString())));
        // Subscribe to chat room and private messages
        firstUserSession.subscribe(createSubHeader(CHAT_ROOM_NAME, FIRST_USER_NAME,false),testStompFrameHandler);
        firstUserSession.subscribe(createSubHeader(CHAT_ROOM_NAME, FIRST_USER_NAME, true), testStompFrameHandler);
        // Send request to obtain a username
        firstUserSession.send("/app/v1/userinfo", new ChatMessageDto(firstClientId, CHAT_ROOM_NAME, "", ""));
        ChatMessageDto outInfoRequestDto = (ChatMessageDto)messageResultQueue.poll(5, TimeUnit.SECONDS);
        assertEquals(firstClientId, outInfoRequestDto.getFrom());
        assertEquals(FIRST_USER_NAME, outInfoRequestDto.getText());
        // The second client connects
        StompSession secondUserSession = stompClient.connect("ws://localhost:" + port + "/v1/chat", testStompSessionHandler).get(10, TimeUnit.SECONDS);
        String secondClientId = (String)messageResultQueue.poll(5, TimeUnit.SECONDS);
        assertThat(secondClientId, is(not(isEmptyString())));
        // Subscribe to chat room and private messages
        secondUserSession.subscribe(createSubHeader(CHAT_ROOM_NAME, SECOND_USER_NAME,false), testStompFrameHandler);
        secondUserSession.subscribe(createSubHeader(CHAT_ROOM_NAME, SECOND_USER_NAME, true), testStompFrameHandler);
        // Send request to obtain a username
        secondUserSession.send("/app/v1/userinfo", new ChatMessageDto(secondClientId, CHAT_ROOM_NAME, "", ""));
        outInfoRequestDto = (ChatMessageDto)messageResultQueue.poll(5, TimeUnit.SECONDS);
        assertEquals(secondClientId, outInfoRequestDto.getFrom());
        assertEquals(SECOND_USER_NAME, outInfoRequestDto.getText());
        // The first user sends message to chat room
        firstUserSession.send("/app/v1/chat", new ChatMessageDto(firstClientId, CHAT_ROOM_NAME, "", "TEST"));
        // Check receive two messages, there should be 2 messages
        ChatMessageDto testMessage = (ChatMessageDto)messageResultQueue.poll(5, TimeUnit.SECONDS);
        assertThat(testMessage.getFrom(), is(not(isEmptyString())));
        assertThat(Arrays.asList(firstClientId, secondClientId), hasItem(testMessage.getFrom()));
        assertEquals(MESSAGE_TEXT, testMessage.getText());

        testMessage = (ChatMessageDto)messageResultQueue.poll(5, TimeUnit.SECONDS);
        assertThat(testMessage.getFrom(), is(not(isEmptyString())));
        assertThat(Arrays.asList(firstClientId, secondClientId), hasItem(testMessage.getFrom()));
        assertEquals(MESSAGE_TEXT, testMessage.getText());

        // Send message from first user to second user
        firstUserSession.send("/app/v1/chat", new ChatMessageDto(firstClientId, CHAT_ROOM_NAME, secondClientId, "TEST"));
        // Check message has correct from client id
        testMessage = (ChatMessageDto)messageResultQueue.poll(5, TimeUnit.SECONDS);
        assertThat(testMessage.getFrom(), is(not(isEmptyString())));
        assertEquals(firstClientId, testMessage.getFrom());
        assertEquals(MESSAGE_TEXT, testMessage.getText());
        // Check there is only one message
        testMessage = (ChatMessageDto)messageResultQueue.poll(5, TimeUnit.SECONDS);
        assertNull(testMessage);

        ResponseEntity<PageDto> result = testRestApiClient.usersInChatRoom(CHAT_ROOM_NAME);
        assertEquals(200, result.getStatusCodeValue());
        assertEquals(2, result.getBody().getTotalElements());
    }

    @Test
    public void ChatRoomAdministratorIntegrationTest() {
        ResponseEntity<ChatRoomDto> chatRoomResponse = testRestApiClient.createTestRoom(CHAT_ROOM_NAME);
        assertNotNull(chatRoomResponse);
        assertEquals(200, chatRoomResponse.getStatusCodeValue());
        assertEquals(CHAT_ROOM_NAME, chatRoomResponse.getBody().getName());

        chatRoomResponse = testRestApiClient.createTestRoom(CHAT_ROOM_NAME + 1);
        assertNotNull(chatRoomResponse);
        assertEquals(200, chatRoomResponse.getStatusCodeValue());
        assertEquals(CHAT_ROOM_NAME + 1, chatRoomResponse.getBody().getName());

        chatRoomResponse = testRestApiClient.createTestRoom(CHAT_ROOM_NAME);
        assertNotNull(chatRoomResponse);
        assertEquals(500, chatRoomResponse.getStatusCodeValue());

        ResponseEntity<PageDto> chatRoomListResponse = testRestApiClient.listRoom();
        assertEquals(200, chatRoomListResponse.getStatusCodeValue());
        assertEquals(2, chatRoomListResponse.getBody().getTotalElements());

        chatRoomResponse = testRestApiClient.deleteTestRoom(CHAT_ROOM_NAME);
        assertNotNull(chatRoomResponse);
        assertEquals(200, chatRoomResponse.getStatusCodeValue());
        assertEquals(CHAT_ROOM_NAME, chatRoomResponse.getBody().getName());

        chatRoomResponse = testRestApiClient.deleteTestRoom(CHAT_ROOM_NAME + 1);
        assertNotNull(chatRoomResponse);
        assertEquals(200, chatRoomResponse.getStatusCodeValue());
        assertEquals(CHAT_ROOM_NAME + 1, chatRoomResponse.getBody().getName());

        chatRoomListResponse = testRestApiClient.listRoom();
        assertEquals(200, chatRoomListResponse.getStatusCodeValue());
        assertEquals(0, chatRoomListResponse.getBody().getTotalElements());
    }

    private static StompHeaders createSubHeader(String chatRoomName, String chatRoomUserName, boolean user) {
        StompHeaders headers = new StompHeaders();
        if(user)
            headers.setDestination("/user/topic/" + CHAT_ROOM_NAME);
        else {
            headers.setDestination("/topic/" + CHAT_ROOM_NAME);
        }
        headers.add("room-user-name", chatRoomUserName);
        return headers;
    }

    private static WebSocketStompClient createStompClient() {
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(transports));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        return stompClient;
    }
}

package com.dimshust.chatapp.service;

import com.dimshust.chatapp.dto.ChatRoomDto;
import com.dimshust.chatapp.dto.PageDto;

public interface ChatRoomService {

    ChatRoomDto createChatRoom(String name);

    ChatRoomDto deleteChatRoom(String name);

    PageDto<ChatRoomDto> listChatRoom(int page, int limit);
}

package com.dimshust.chatapp.system;

import com.dimshust.chatapp.service.ChatRoomUserService;
import com.google.common.base.Strings;
import lombok.extern.log4j.Log4j2;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class UserHeaderChannelInterceptor implements ChannelInterceptor {

    private final ChatRoomUserService chatRoomUserService;

    public UserHeaderChannelInterceptor(ChatRoomUserService chatRoomUserService) {
        this.chatRoomUserService = chatRoomUserService;
    }

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {

        final StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        if (StompCommand.CONNECT == accessor.getCommand()) {
            log.info("connection request from us`er:{}", accessor.getUser().getName());
        } else if (StompCommand.DISCONNECT == accessor.getCommand()) {
            final String clientId = accessor.getUser().getName();
            log.info("disconnect request from client id:{}", clientId);
            chatRoomUserService.deleteUser(clientId);
        } else if (StompCommand.SUBSCRIBE == accessor.getCommand()) {
            final String username = accessor.getFirstNativeHeader("room-user-name");
            final String roomId = roomIdFromDestination(accessor.getDestination());
            final String clientId = accessor.getUser().getName();
            log.info("subscription request from user id:{}, destination:{}, room id:{}, prefered name:{}"
                    , clientId
                    , accessor.getDestination()
                    , roomId
                    , username);
            chatRoomUserService.registerUserInChatRoom(clientId,
                    roomId,
                    username);
        } else if (StompCommand.UNSUBSCRIBE == accessor.getCommand()) {
            final String roomId = roomIdFromDestination(accessor.getDestination());
            final String clientId = accessor.getUser().getName();
            log.info("unsubscribe request from client id:{}, room id:{}", clientId, roomId);
            chatRoomUserService.deleteUserFromChatRoom(clientId, roomId);
        }
        return message;
    }

    static String roomIdFromDestination(String destination) {
        if(Strings.isNullOrEmpty(destination)) {
            throw new IllegalArgumentException("destination is empty");
        }
        String[] destinationParts = destination.split("/");
        return destinationParts[destinationParts.length - 1];
    }
}

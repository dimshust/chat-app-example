package com.dimshust.chatapp.controller;

import com.dimshust.chatapp.dto.ChatRoomDto;
import com.dimshust.chatapp.service.ChatRoomService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Log4j2
@Controller
@RequestMapping(path="/api/v1/administrator/chatroom/")
public class ChatRoomAdministratorController {
    private final ChatRoomService chatRoomService;

    public ChatRoomAdministratorController(ChatRoomService chatRoomService) {
        this.chatRoomService = chatRoomService;
    }

    @PostMapping(path = "/create")
    @ResponseBody
    @ApiOperation(value = "Create chat room")
    public ChatRoomDto createChatRoom(@ApiParam("chat room name") @RequestParam("name") String name) {
        return chatRoomService.createChatRoom(name);
    }

    @GetMapping(path = "/delete")
    @ResponseBody
    @ApiOperation(value = "Delete chat room")
    public ChatRoomDto deleteChatRoom(@ApiParam("chat room name") @RequestParam("name") String name) {
        return chatRoomService.deleteChatRoom(name);
    }

}

package com.dimshust.chatapp.controller;

import com.dimshust.chatapp.dto.ChatRoomDto;
import com.dimshust.chatapp.dto.PageDto;
import com.dimshust.chatapp.service.ChatRoomService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Log4j2
@Controller
@RequestMapping(path="/api/v1/user/chatroom")
public class ChatRoomController {

    private final ChatRoomService chatRoomService;

    public ChatRoomController(ChatRoomService chatRoomService) {
        this.chatRoomService = chatRoomService;
    }

    @GetMapping(path = "/list")
    @ResponseBody
    @ApiOperation(value = "List of chat rooms")
    public PageDto<ChatRoomDto> listChatRooms(
            @RequestParam(value = "page", defaultValue = "0") int page
            , @RequestParam(value = "limit", defaultValue = "100") int limit) {
        return chatRoomService.listChatRoom(page, limit);
    }
}

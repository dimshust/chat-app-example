package com.dimshust.chatapp.controller;

import com.dimshust.chatapp.dto.*;
import com.dimshust.chatapp.service.ChatRoomUserService;
import com.google.common.base.Strings;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class MessageController {

    private static final String TOPIC_NAME = "/topic/";

    private SimpMessagingTemplate template;
    private ChatRoomUserService chatRoomUserService;

    public MessageController(SimpMessagingTemplate template, ChatRoomUserService chatRoomUserService) {
        this.template = template;
        this.chatRoomUserService = chatRoomUserService;
    }

    /**
     * Route message to a topic (public room),
     * or if message contains destination (to attribute), message goes only to the user.
     */
    @MessageMapping("/v1/chat")
    public void processChatMessages(ChatMessageDto message, SimpMessageHeaderAccessor headerAccessor) {
        if(Strings.isNullOrEmpty(message.getTo())) {
            template.convertAndSend(
                    TOPIC_NAME + message.getChatRoom()
                    , new ChatMessageDto(message.getFrom(), message.getChatRoom(), "", message.getText()));
        } else {
            template.convertAndSendToUser(
                    message.getTo(),
                    TOPIC_NAME + message.getChatRoom()
                    , new ChatMessageDto(message.getFrom(), message.getChatRoom(), "", message.getText()));
        }
    }

    @MessageMapping("/v1/userinfo")
    public void processUserInfoRequest(ChatMessageDto message) {
        ChatRoomUserDto user = chatRoomUserService.getUserInChatRoom(message.getChatRoom(), message.getFrom());
        template.convertAndSendToUser(
                message.getFrom(),
                TOPIC_NAME + message.getChatRoom()
                , new ChatMessageDto(message.getFrom(), message.getChatRoom(), "", user.getName()));
    }
}

package com.dimshust.chatapp.main.repository;

import com.dimshust.chatapp.main.model.ChatRoom;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChatRoomRepository extends PagingAndSortingRepository<ChatRoom, String> {
    Optional<ChatRoom> findByName(String name);
    void deleteByName(String name);
}

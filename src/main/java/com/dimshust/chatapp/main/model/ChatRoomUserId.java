package com.dimshust.chatapp.main.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(of = {"clientId", "chatRoomName"})
public class ChatRoomUserId implements Serializable {
    private String clientId;
    private String chatRoomName;

    public ChatRoomUserId(String clientId, String chatRoomName) {
        this.clientId = clientId;
        this.chatRoomName = chatRoomName;
    }

    public ChatRoomUserId(){
    }
}

package com.dimshust.chatapp.dto;

import com.dimshust.chatapp.main.model.ChatRoom;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChatRoomDto {
    private final String name;
    private final List<ChatRoomUserDto> users;

    public static ChatRoomDto fromEntity(ChatRoom entity) {
        return new ChatRoomDto(entity.getName(), ChatRoomUserDto.fromEntity(entity.getUsers()));
    }

    public static List<ChatRoomDto> fromEntity(List<ChatRoom> entities) {
        if(entities == null) {
            return Collections.emptyList();
        }
        return entities.stream().map(ChatRoomDto::fromEntity).collect(Collectors.toList());
    }

    public static ChatRoom toEntity(ChatRoomDto dto) {
        ChatRoom entity = new ChatRoom();
        entity.setName(dto.getName());
        return entity;
    }

    public static List<ChatRoom> toEntity(List<ChatRoomDto> dtos) {
        if(dtos == null) {
            return Collections.emptyList();
        }
        return dtos.stream().map(ChatRoomDto::toEntity).collect(Collectors.toList());
    }
}

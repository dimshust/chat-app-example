package com.dimshust.chatapp.dto;

import com.dimshust.chatapp.main.model.ChatRoomUser;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChatRoomUserDto {

    private final String clientId;
    private final String chatRoomName;
    private final String name;

    public static ChatRoomUserDto fromEntity(ChatRoomUser entity) {
        return new ChatRoomUserDto(String.valueOf(entity.getClientId()), String.valueOf(entity.getChatRoomName()), entity.getName());
    }

    public static List<ChatRoomUserDto> fromEntity(List<ChatRoomUser> entities) {
        if(entities == null) {
            return Collections.emptyList();
        }
        return entities.stream().map(ChatRoomUserDto::fromEntity).collect(Collectors.toList());
    }

    public static ChatRoomUser toEntity(ChatRoomUserDto dto) {
        ChatRoomUser entity = new ChatRoomUser();
        entity.setClientId(dto.getClientId());
        entity.setChatRoomName(dto.getChatRoomName());
        entity.setName(dto.getName());
        return entity;
    }

    public static List<ChatRoomUser> toEntity(List<ChatRoomUserDto> dtos) {
        if(dtos == null) {
            return Collections.emptyList();
        }
        return dtos.stream().map(ChatRoomUserDto::toEntity).collect(Collectors.toList());
    }
}
